#!/bin/bash
echo "Starting to pull image ..."
touch /tmp/foo-activated
var=$(docker ps | grep $1 | wc -l)
echo Number of running containers using the $1 image: $var
while [ $var -gt 0 ]
do
echo waiting for container exit...
sleep 5
var=$(docker ps | grep $1 | wc -l)
done
docker pull $1
