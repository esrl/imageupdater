FROM ubuntu
RUN apt-get update -y
RUN apt-get install docker.io -y
ENV imgName test
COPY updateImage.sh /usr/local/bin/updateImage.sh 
RUN chmod +x /usr/local/bin/updateImage.sh 
CMD updateImage.sh $imgName
